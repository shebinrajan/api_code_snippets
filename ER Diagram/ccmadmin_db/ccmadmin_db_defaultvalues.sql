-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmadmin_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `defaultvalues`
--

DROP TABLE IF EXISTS `defaultvalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `defaultvalues` (
  `DefaultID` int NOT NULL AUTO_INCREMENT,
  `DefaultName` varchar(45) DEFAULT NULL,
  `Defaultvalue1` decimal(10,0) DEFAULT NULL,
  `Defaultvalue2` datetime DEFAULT NULL,
  `Defaultvalue3` double DEFAULT NULL,
  `Defaultvalue4` float DEFAULT NULL,
  `Defaultvalue5` date DEFAULT NULL,
  `Defaultvalue6` tinyint DEFAULT NULL,
  PRIMARY KEY (`DefaultID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defaultvalues`
--

LOCK TABLES `defaultvalues` WRITE;
/*!40000 ALTER TABLE `defaultvalues` DISABLE KEYS */;
INSERT INTO `defaultvalues` VALUES (2,'update',NULL,NULL,NULL,NULL,NULL,NULL),(3,'newvalue',124,'2021-08-17 13:42:49',NULL,NULL,NULL,NULL),(4,'default',249,'2021-08-17 20:41:59',289.45,NULL,NULL,NULL),(5,'default1',500,'2021-08-17 20:49:15',123.6556665,123.235,NULL,NULL),(6,'weareone',56677,'2021-08-20 19:27:01',1235.68789434,12345.5,'2021-08-20',NULL);
/*!40000 ALTER TABLE `defaultvalues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:26
