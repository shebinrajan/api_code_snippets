-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmadmin_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `organizationusermapping`
--

DROP TABLE IF EXISTS `organizationusermapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organizationusermapping` (
  `Org_ID` int NOT NULL,
  `User_ID` int NOT NULL,
  `Role_ID` int NOT NULL,
  `IsActive` smallint NOT NULL,
  PRIMARY KEY (`Org_ID`,`User_ID`,`Role_ID`),
  KEY `index2` (`User_ID`) /*!80000 INVISIBLE */,
  KEY `index3` (`Role_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizationusermapping`
--

LOCK TABLES `organizationusermapping` WRITE;
/*!40000 ALTER TABLE `organizationusermapping` DISABLE KEYS */;
INSERT INTO `organizationusermapping` VALUES (0,65,3,1),(0,66,3,1),(0,67,3,1),(0,110,3,1),(1,2,2,1),(1,45,2,1),(1,74,3,1),(1,75,3,1),(1,76,3,1),(1,77,3,1),(1,78,3,1),(1,79,3,1),(1,80,3,1),(1,81,3,1),(1,84,3,1),(1,104,3,1),(1,105,3,1),(1,107,3,1),(1,108,3,1),(2,4,2,1),(2,68,3,1),(2,82,2,1),(2,89,3,1),(2,90,3,1),(3,11,2,1),(4,12,2,1),(5,14,2,1),(7,16,2,1),(8,27,3,1),(12,50,2,1),(12,63,2,1),(12,71,3,1),(13,22,2,1),(14,31,2,1),(15,36,2,1),(15,37,2,1),(15,64,2,1),(16,39,2,1),(17,46,2,1),(18,47,2,1),(18,70,3,1),(18,85,3,1),(18,92,3,1),(19,49,2,1),(21,53,2,1),(21,55,2,1),(21,56,2,1),(21,57,2,1),(21,62,2,1),(21,69,2,1),(26,87,2,1),(26,88,2,1),(26,91,2,1),(27,96,2,1),(27,102,2,1),(27,111,3,1),(27,113,3,1),(27,114,3,1),(27,118,2,1),(27,119,2,1),(27,128,3,1),(28,97,2,1),(28,98,3,1),(28,101,3,1),(28,103,3,1),(28,106,3,1),(28,117,3,1),(29,116,2,1),(32,122,2,1),(32,124,3,1),(33,123,2,1),(35,126,2,1);
/*!40000 ALTER TABLE `organizationusermapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:26
