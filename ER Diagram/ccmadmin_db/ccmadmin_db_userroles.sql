-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmadmin_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `userroles`
--

DROP TABLE IF EXISTS `userroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userroles` (
  `UserId` int NOT NULL,
  `RoleId` int NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IX_UserRoles_RoleId` (`RoleId`),
  CONSTRAINT `FK_UserRoles_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `role` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_UserRoles_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userroles`
--

LOCK TABLES `userroles` WRITE;
/*!40000 ALTER TABLE `userroles` DISABLE KEYS */;
INSERT INTO `userroles` VALUES (1,1),(19,1),(92,1),(93,1),(2,2),(4,2),(11,2),(12,2),(14,2),(15,2),(16,2),(22,2),(31,2),(32,2),(36,2),(37,2),(38,2),(39,2),(45,2),(47,2),(49,2),(50,2),(53,2),(55,2),(56,2),(57,2),(62,2),(63,2),(64,2),(69,2),(82,2),(87,2),(88,2),(91,2),(96,2),(97,2),(102,2),(116,2),(118,2),(119,2),(122,2),(123,2),(126,2),(17,3),(65,3),(66,3),(67,3),(68,3),(70,3),(71,3),(75,3),(76,3),(77,3),(78,3),(79,3),(80,3),(81,3),(84,3),(85,3),(89,3),(90,3),(98,3),(101,3),(103,3),(104,3),(105,3),(106,3),(107,3),(108,3),(110,3),(111,3),(113,3),(114,3),(117,3),(124,3),(128,3),(3,4),(5,4),(6,4),(7,4),(8,4),(9,4),(10,4),(18,4),(20,4),(21,4),(23,4),(24,4),(25,4),(26,4),(27,4),(28,4),(29,4),(30,4),(33,4),(34,4),(35,4),(40,4),(41,4),(42,4),(43,4),(44,4),(48,4),(51,4),(52,4),(54,4),(58,4),(59,4),(60,4),(61,4),(72,4),(73,4),(83,4),(86,4),(94,4),(95,4),(99,4),(100,4),(109,4),(112,4),(115,4),(120,4),(121,4),(125,4),(127,4),(129,4);
/*!40000 ALTER TABLE `userroles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:26
