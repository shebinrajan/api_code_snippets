-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmadmin_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `useauthrtokens`
--

DROP TABLE IF EXISTS `useauthrtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `useauthrtokens` (
  `UserId` int NOT NULL,
  `LoginProvider` varchar(45) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Value` text,
  `expires` datetime DEFAULT NULL,
  `IpAddress` varchar(45) DEFAULT NULL,
  `Status` int NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useauthrtokens`
--

LOCK TABLES `useauthrtokens` WRITE;
/*!40000 ALTER TABLE `useauthrtokens` DISABLE KEYS */;
INSERT INTO `useauthrtokens` VALUES (1,'Email','commercium@mailinator.com','460598',NULL,'103.151.188.15',0),(2,'Email','manojvijayanaluva@gmail.com','228821',NULL,'137.97.65.138',0),(3,'Email','aravindr3@gmail.com','634060',NULL,'127.0.0.1',0),(19,'Email','jaimont365@gmail.com','623239',NULL,'103.147.208.250',1),(20,'Email','sreeharimanojsreelakam@gmail.com','024515',NULL,'178.248.115.52',0),(47,'Email','jaimont247@gmail.com','666456',NULL,'171.61.173.63',0),(50,'Email','rupesh@mailinator.com','191709',NULL,'2.51.91.40',0),(51,'Email','thomas@mailinator.com','465090',NULL,'2.51.91.40',0),(53,'Email','kp@mailinator.com','398500',NULL,'2.51.91.40',0),(68,'Email','userSalesRep1@example.com','328794',NULL,'127.0.0.1',0),(69,'Email','sam@gmail.com','843883',NULL,'103.154.54.11',1),(75,'Email','ccmerpdemo2@gmail.com','292231',NULL,'127.0.0.1',1),(79,'Email','vineetha.m@agmasystems.com','914281',NULL,'178.248.115.85',0),(82,'Email','manojvijayanaluva@gmail.com','997236',NULL,'157.44.143.210',0),(86,'Email','aravind@yopmail.com','379523',NULL,'103.151.189.12',0),(88,'Email','usercatch92@gmail.com','294720',NULL,'103.151.189.12',0),(91,'Email','ram@yopmail.com','842674',NULL,'103.154.54.14',0),(93,'Email','jaimon.thomas@cubettech.com','315903',NULL,'103.154.54.72',0),(96,'Email','qa@yopmail.com','749466',NULL,'103.151.189.10',0),(98,'Email','jaimont247@gmail.com','629386',NULL,'103.147.208.247',0),(99,'Email','first@yopmail.com','602392',NULL,'103.154.54.11',0),(100,'Email','city@yopmail.com','973520',NULL,'103.151.188.2',0),(101,'Email','jaimon.thomas@cubettech.com','738117',NULL,'103.147.208.247',0),(107,'Email','ccmerpdemo@gmail.com','526571',NULL,'206.42.64.212',1),(108,'Email','ccmerpdemo@gmail.com','042962',NULL,'206.42.64.212',1),(109,'Email','dforjunk@gmail.com','559864',NULL,'171.61.173.63',0),(113,'Email','market@yopmail.com','847663',NULL,'103.151.188.2',0),(115,'Email','thinkercards@gmail.com','434416',NULL,'117.202.116.50',1),(116,'Email','thinkercards@gmail.com','100043',NULL,'117.202.116.50',0),(117,'Email','triprequestuat@gmail.com','223751',NULL,'103.147.208.232',0),(118,'Email','tiger@yopmail.com','913813',NULL,'103.154.54.12',0),(120,'Email','send1@yopmail.com','674990',NULL,'103.154.54.12',0),(121,'Email','jaimont@gmail.com','904208',NULL,'171.61.173.63',0),(122,'Email','krishna.priya@cubettech.com','924291',NULL,'157.45.160.14',0),(124,'Email','salesrepcubet_test_06@mailinator.com','596346',NULL,'157.45.160.14',0),(126,'Email','krishna@mailinator.com','817840',NULL,'157.45.190.58',0),(127,'Email','priya@mailinator.com','440277',NULL,'157.45.190.58',0);
/*!40000 ALTER TABLE `useauthrtokens` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:27
