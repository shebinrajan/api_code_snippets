-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmadmin_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organization` (
  `Org_ID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `CountryID` int DEFAULT NULL,
  `Currency_ID` int DEFAULT NULL,
  `ContactPerson` varchar(45) DEFAULT NULL,
  `ContactNumber` varchar(15) DEFAULT NULL,
  `VATID` varchar(15) DEFAULT NULL,
  `TaxWording` varchar(45) DEFAULT NULL,
  `DBConnection` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedByUser` int DEFAULT NULL,
  `CreatedByProgram` varchar(45) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int DEFAULT NULL,
  `LastModifiedByProgram` varchar(45) DEFAULT NULL,
  `ExternalReference` varchar(45) DEFAULT NULL,
  `Comments` varchar(150) DEFAULT NULL,
  `Zipcode` varchar(15) DEFAULT NULL,
  `IsActive` int DEFAULT NULL,
  PRIMARY KEY (`Org_ID`),
  KEY `index2` (`CountryID`) /*!80000 INVISIBLE */,
  KEY `index3` (`Currency_ID`),
  CONSTRAINT `fk_organization_country` FOREIGN KEY (`CountryID`) REFERENCES `country_master` (`CountryID`),
  CONSTRAINT `fk_organization_currency` FOREIGN KEY (`Currency_ID`) REFERENCES `currency_master` (`CurrencyID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
INSERT INTO `organization` VALUES (1,'Dream Land Corporation','Bristol','Dan Street','London','WLq',1,1,'William Bon','9746320067','GB234239669','sxsdsd',NULL,'2021-07-04 20:50:09',1,'dadad','2021-07-27 00:50:18',1,NULL,'adad','adad','123444',1),(2,'manoj2','ewrwer','werwer','cochin','Kerala (Keralam)',1,1,'werwer','09847483390','cbxbc','string',NULL,'2021-07-04 15:20:38',1,'Organisation1','2021-07-27 15:07:34',2,'asasas','Organisation1','Organisation1','680734',1),(3,'WonderLand','London','343 WL','United Kingdom','United Kingdom',1,1,'Elton John ','9847483390','GB112669669','',NULL,'2021-07-04 15:46:14',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'685533',1),(4,'Carl Brothers','Wales','D Town','Manchester','UK',1,1,'George W','9847483390','GB524669633','',NULL,'2021-07-04 15:49:35',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'685533',1),(5,'Bunes Company','New Wales','South Street','London','London',1,1,'Tom Patrik','9847483390','GB449669669','',NULL,'2021-07-04 15:51:04',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'685533',1),(6,'Bates Inc.','234,D Street','SL','New Hamshire','London',1,1,'Juan Song','9847483390','GB550000669','',NULL,'2021-07-04 15:52:53',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'691601',1),(7,'Royal Hospital','#12 , WL Col','West End','Bristol','London',1,1,'Dan Martin','9847483390','GB569669669','',NULL,'2021-07-04 15:54:09',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'68553',1),(8,'J & K Company','33, MEA Junction','D Block','Bristol','London',1,1,'Edward Jr','09847483390','GB616669669','',NULL,'2021-07-04 16:03:03',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'66664',1),(9,'Royal Hospital Chelsea7','Royal Hospital Chelsea7','Royal Hospital Chelsea7','Royal Hospital Chelsea7','Royal Hospital Chelsea7',1,1,'Harison Gilbert','9847483390','GB133669669','',NULL,'2021-07-04 16:06:05',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'68553',1),(10,'Dream Cloud PLC','CABLE DRIVE, WALSALL, WEST MIDLANDS, WS2 7BN','',' WALSALL','WS2 7BN',1,1,'Robert W Goodman','9885545454','GB945669669','',NULL,'2021-07-05 09:17:39',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'325456',1),(11,'Dukes Corp','#1212 , SL Town','WSL','Walsal','WL2',1,1,'Chung Wee Lee','9845978589','GB122669669','',NULL,'2021-07-07 15:08:33',1,NULL,'2021-07-27 00:52:53',1,NULL,NULL,NULL,'12312',1),(12,'Town Plans','81, D Town, K','Down Town','Walsal','WL',1,1,'Richard ','9456123789','GB329669003','',NULL,'2021-07-08 09:18:23',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'12312',1),(13,'Howra Diamonds','89 Street # 12,  ','Park Road','London','London',1,1,'Sheik M','09847483390','GB449663366','',NULL,'2021-07-09 02:09:18',1,NULL,'2021-07-15 01:48:56',1,NULL,NULL,NULL,'685533',1),(14,'Quick Doc','Street # 12, BD Township','Elk Road','Bristol','London',1,1,'Julia Wilson','09847483390','GB139269669','',NULL,'2021-07-09 04:09:38',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'685533',1),(15,'HOMESERVE PLC','ABLE DRIVE, WALSALL, WEST MIDLANDS, WS2 7BN','','WALSALL','WEST MIDLANDS',1,1,'Robert','9995556666','GB559669669','',NULL,'2021-07-09 05:03:03',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'WS2 7BN',1),(16,'ABC Corporation','#22,WallStreet','','London','London',1,1,'William','8884449999','GB898988898','',NULL,'2021-07-09 05:11:12',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'565656',1),(17,'TESCO PLC','FAO MICHELE HOOD, TESCO PLC','CABLE DRIVE, WALSALL,','WALSALL,','WEST MIDLANDS',1,1,'Garry Livingstone','7884555666','GB220430231','',NULL,'2021-07-09 06:19:30',1,NULL,'2021-07-27 00:50:32',1,NULL,NULL,NULL,'WS2 7BN',1),(18,'Channel Shop','Deo Bulding','CABLE DRIVE, WALSALL,','WALSALL,','WEST MIDLANDS',1,1,'Peter Livingstone','07884555666','GB78867668','',NULL,'2021-07-09 06:22:48',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'WS2 7BN',1),(19,'New Tech','New zealand ','','Auckland','Ackland',1,1,'Sam','9879769767','345','',NULL,'2021-07-12 10:04:23',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'56776',0),(20,'manoj2','ewrwer','werwer','cochin','Kerala (Keralam)',1,1,'werwer','09847483390','cbxbc','string',NULL,'2021-07-15 01:44:32',1,NULL,'2021-07-27 15:15:38',2,'asasas',NULL,NULL,'680734',1),(21,'cubet','Kochi','','Ernakulam','kochi',1,1,'Arun','0943093443','90','',NULL,'2021-07-19 14:34:29',1,NULL,'2021-07-20 13:33:36',1,NULL,NULL,NULL,'345',1),(22,'Dream Land Corporation2','Poovathur Thekkathil House','Chonamchira','Kollam','Kerala',1,1,'manoj','09746320067','wrwer','',NULL,'2021-07-20 10:55:55',1,NULL,'2021-07-20 10:56:03',1,NULL,NULL,NULL,'691601',1),(23,'organisation-manoj1','cochin','ad2','cochin','Kerala (Keralam)',1,1,'manoj','09847483390','2323','',NULL,'2021-07-21 02:33:03',1,NULL,'2021-07-21 02:33:29',1,NULL,NULL,NULL,'685533',1),(24,'rrrr','zxzxzx','zxzxzx','zxzx','zxzx',1,1,'zczczc','7654324456','323232','xzxzx',NULL,'2021-07-21 20:16:14',3,'zxzxzx','0001-01-01 00:00:00',0,NULL,'zxzxz','zxzxz','234567',1),(25,'Orgtest','zxzxzx','zxzxzx','zxzxzx','zxzxzx',1,1,'sdsdsd','9876545678','asasasas','asas',NULL,'2021-07-21 14:56:10',2,'dwdwdwd','0001-01-01 00:00:00',0,NULL,'wdwd','wdwd','234567',0),(26,'QA ','address','address2','kochi','kerala',1,1,'Midhun','6967976997','67899','',NULL,'2021-07-28 05:17:36',1,NULL,'2021-07-28 05:44:29',1,NULL,NULL,NULL,'4547',0),(27,'QA org','address 1','address 2','kochi','kerala',1,1,'Midhun','8798798767','1234','',NULL,'2021-08-02 06:34:32',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'45678',1),(28,'Beta Systems India Pvt. Ltd','#8/44K, Noa\'s Ark, First Floor, NH47,','ANGAMALY SOUTH Ernakulam KL','Ernakulam','Kerala',1,1,'Jaimon Thomas','+919496100854','232444444444','',NULL,'2021-08-02 07:05:36',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'683573',1),(29,'thinkercards','Home Name','Home Address1','Erknakulam','Kerala',1,1,'martin','9846577570','6835700','tax text',NULL,'2021-08-03 15:54:38',1,'program1','0001-01-01 00:00:00',0,NULL,'reference1','text conment','6832572',1),(30,'wwdwdw','strdwding','strwdwing','stwdwdring','stwdwdring',1,1,'sdsd','2345678890','wdwdw','wdwd',NULL,'2021-08-06 01:57:15',2,'sdsdsdsd','0001-01-01 00:00:00',0,NULL,'sdsd','dssd','23456',1),(31,'wwdssswdw','strdwding','strwdwing','stwdwdring','stwdwdring',1,1,'sdsd','2345678890','wdwdw','wdwd',NULL,'2021-08-06 01:59:05',2,'sdsdsdsd','0001-01-01 00:00:00',0,NULL,'sdsd','dssd','23456',1),(32,'Cubet_Test_ORG','XYZ','XYZ','gdjhgjh','Texas',1,3,'KP_Cubet','9847112456','123456','',NULL,'2021-08-06 07:47:11',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'123345',1),(33,'Cubet_Test_Org_06','XYZ','XYZ','Test_city','Texas',1,3,'Rupesh','9847112456','GB12345','',NULL,'2021-08-06 08:43:21',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'12345',1),(34,'Cubet_10','XYZ','XYZ','XYZ','XYZ',1,1,'Cubet Admin','9847112345','CB0012','',NULL,'2021-08-10 11:56:42',1,NULL,'0001-01-01 00:00:00',0,NULL,NULL,NULL,'560024',1),(35,'Cubet_Test_10','Carnival Inforpark, Kakkanad','Carnival Infopar, kakkanad','Cochin','Kerala',1,5,'Cubet Admin','9874123568','CB0012','',NULL,'2021-08-10 11:58:19',1,NULL,'2021-08-10 12:45:50',1,NULL,NULL,NULL,'54123',1);
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:27
