-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmtransdb
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `keymaster`
--

DROP TABLE IF EXISTS `keymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keymaster` (
  `KeyId` int NOT NULL,
  `OrgId` int NOT NULL,
  `Value` bigint NOT NULL,
  `Description` varchar(45) NOT NULL,
  PRIMARY KEY (`KeyId`,`OrgId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keymaster`
--

LOCK TABLES `keymaster` WRITE;
/*!40000 ALTER TABLE `keymaster` DISABLE KEYS */;
INSERT INTO `keymaster` VALUES (1,1,15,'Order No'),(1,2,14,'OrderNumber'),(1,3,0,'OrderNumber'),(1,4,0,'OrderNumber'),(1,5,0,'Order No'),(1,6,0,'OrderNumber'),(1,7,0,'OrderNumber'),(1,8,0,'OrderNumber'),(1,9,0,'OrderNumber'),(1,10,0,'OrderNumber'),(1,11,0,'OrderNumber'),(1,12,0,'OrderNumber'),(1,13,0,'OrderNumber'),(1,14,0,'OrderNumber'),(1,15,0,'OrderNumber'),(1,16,0,'OrderNumber'),(1,17,0,'OrderNumber'),(1,18,8,'OrderNumber'),(1,19,0,'OrderNumber'),(1,20,0,'OrderNumber'),(1,21,0,'OrderNumber'),(1,22,0,'OrderNumber'),(1,23,0,'OrderNumber'),(1,24,0,'OrderNumber'),(1,25,0,'OrderNumber'),(1,26,0,'OrderNumber'),(1,27,0,'OrderNumber'),(1,28,1,'OrderNumber'),(1,29,0,'OrderNumber'),(1,30,0,'OrderNumber'),(1,31,0,'Order No'),(1,32,0,'Order No'),(1,33,0,'Order No'),(1,34,0,'Order No'),(1,35,0,'Order No');
/*!40000 ALTER TABLE `keymaster` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:30
