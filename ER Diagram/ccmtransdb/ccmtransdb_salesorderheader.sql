-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmtransdb
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `salesorderheader`
--

DROP TABLE IF EXISTS `salesorderheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salesorderheader` (
  `SOHdrId` int NOT NULL AUTO_INCREMENT,
  `OrgId` int NOT NULL,
  `CustomerId` int NOT NULL,
  `SONo` varchar(20) NOT NULL,
  `SODate` date NOT NULL,
  `ExpectedDate` date NOT NULL,
  `Remarks` varchar(250) DEFAULT NULL,
  `ShippingAddress1` varchar(100) DEFAULT NULL,
  `ShippingAddress2` varchar(100) DEFAULT NULL,
  `ShippingCity` varchar(45) DEFAULT NULL,
  `ShippingState` varchar(45) DEFAULT NULL,
  `ShippingCountry` int DEFAULT NULL,
  `ShippingZipCode` varchar(10) DEFAULT NULL,
  `BillingAddress1` varchar(100) DEFAULT NULL,
  `BillingAddress2` varchar(100) DEFAULT NULL,
  `BillingCity` varchar(45) DEFAULT NULL,
  `BillingState` varchar(45) DEFAULT NULL,
  `BillingCountry` int DEFAULT NULL,
  `BillingZipCode` varchar(10) DEFAULT NULL,
  `CurrencyId` int DEFAULT NULL,
  `StatusId` int DEFAULT NULL,
  PRIMARY KEY (`SOHdrId`),
  KEY `index2` (`OrgId`),
  KEY `index3` (`CustomerId`) /*!80000 INVISIBLE */,
  KEY `index4` (`CurrencyId`),
  KEY `index5` (`ShippingCountry`),
  KEY `index6` (`BillingCountry`) /*!80000 INVISIBLE */,
  KEY `index7` (`CurrencyId`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesorderheader`
--

LOCK TABLES `salesorderheader` WRITE;
/*!40000 ALTER TABLE `salesorderheader` DISABLE KEYS */;
INSERT INTO `salesorderheader` VALUES (1,1,22,'SONO102200000002','2021-08-02','2021-08-23',NULL,'sdadasd','saddadad','asdasdasd','adasda',1,'123456','cscsc','sccscsc','scscsc','scscs',1,'123456',1,10),(2,2,44,'20000000001','2021-08-06','2021-08-06',NULL,'wer','ewr','werwe','ewr',1,'wer','erw','wer','wer','wer',1,'wer',1,10),(4,2,44,'20000000002','2021-08-06','2021-08-06',NULL,'wer','ewr','werwe','ewr',1,'wer','erw','wer','wer','wer',1,'wer',1,10),(5,2,44,'20000000003','2021-08-05','2021-08-05',NULL,'wer','ewr','werwe','ewr',1,'wer','erw','wer','wer','wer',1,'wer',1,10),(6,1,3,'10000000003','2021-08-07','2021-08-07',NULL,'Royal Hospital Chelsea','Royal Hospital Chelsea','Royal Hospital Chelsea','Royal Hospital Chelsea',1,'4535','Royal Hospital Chelsea','Royal Hospital Chelsea','Royal Hospital Chelsea','Royal Hospital Chelsea',1,'43534543',1,10),(7,1,20,'10000000004','2021-08-07','2021-08-07',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(8,1,20,'10000000005','2021-08-11','2021-08-31',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(9,1,20,'10000000006','2021-08-11','2021-08-31',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(10,1,20,'10000000007','2021-01-11','2021-01-31',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(11,1,20,'10000000008','2021-01-11','2021-01-31',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(12,1,20,'10000000009','2021-01-11','2021-01-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(13,1,20,'10000000010','2021-01-15','2021-01-01',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(17,2,20,'20000000004','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(18,2,20,'20000000005','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(19,2,20,'20000000006','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(20,2,20,'20000000007','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(21,2,20,'20000000008','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(23,2,20,'20000000009','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(25,2,20,'20000000010','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(27,2,20,'20000000011','2021-08-11','2021-08-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(28,2,48,'20000000012','2021-08-11','2021-08-11',NULL,'asasa','asas','asasas','asass',1,'1233','cochin','','cochin','Kerala (Keralam)',1,'685533',1,10),(29,2,48,'20000000013','2021-08-11','2021-08-11',NULL,'asasa','asas','asasas','asass',1,'1233','cochin','','cochin','Kerala (Keralam)',1,'685533',1,10),(30,2,48,'20000000014','2021-08-11','2021-08-11',NULL,'asasa','asas','asasas','asass',1,'1233','cochin','','cochin','Kerala (Keralam)',1,'685533',1,10),(31,1,20,'10000000011','2021-01-12','2021-01-28',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(32,1,20,'10000000012','2021-01-11','2021-01-12',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(33,1,20,'10000000013','2021-01-11','2021-01-14',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(34,1,20,'10000000014','2021-01-11','2021-01-11',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(35,28,53,'28000000001','2021-01-11','2021-01-12',NULL,'#8/44K, Noa\'s Ark, First Floor, NH47,','ANGAMALY SOUTH Ernakulam KL','Ernakulam','Kerala',4,'683573','#8/44K, Noa\'s Ark, First Floor, NH47,','ANGAMALY SOUTH Ernakulam KL','Ernakulam','Kerala',3,'683573',1,10),(38,18,18,'18000000001','2021-01-12','2021-01-05',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(39,18,0,'18000000002','2021-01-13','2021-01-13',NULL,'Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(41,1,20,'10000000015','2021-01-13','2021-01-20',NULL,'cochin','asas','cochin','Kerala (Keralam)',1,'685533','xcxcxc','xcxcxc','xcxc','xcxcxc',1,'zxzxzxzx',1,10),(42,18,18,'18000000003','2021-01-13','2021-01-13',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(43,18,18,'18000000004','2021-01-13','2021-01-20',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(44,18,18,'18000000005','2021-01-13','2021-01-13',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(45,18,18,'18000000006','2021-01-13','2021-01-12',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(46,18,18,'18000000007','2021-01-13','2021-01-14',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10),(47,18,18,'18000000008','2021-01-13','2021-01-14',NULL,'Gren MICHELE HOOD, ','New Street, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN','Care Bulding','CABLE DRIVE, WALSALL,','WALSALL,','Walsal',1,'WS2 7BN',1,10);
/*!40000 ALTER TABLE `salesorderheader` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:29
