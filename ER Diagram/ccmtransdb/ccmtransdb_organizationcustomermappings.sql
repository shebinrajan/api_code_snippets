-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ccmtransdb
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `organizationcustomermappings`
--

DROP TABLE IF EXISTS `organizationcustomermappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organizationcustomermappings` (
  `Org_ID` int NOT NULL,
  `CustomerID` int NOT NULL,
  `User_ID` int NOT NULL,
  `IsActive` smallint NOT NULL,
  `SalesRepId` int DEFAULT NULL,
  PRIMARY KEY (`Org_ID`,`CustomerID`),
  KEY `index` (`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizationcustomermappings`
--

LOCK TABLES `organizationcustomermappings` WRITE;
/*!40000 ALTER TABLE `organizationcustomermappings` DISABLE KEYS */;
INSERT INTO `organizationcustomermappings` VALUES (0,49,0,0,110),(1,1,73,1,12),(1,2,2,1,1),(1,3,3,1,0),(1,9,0,0,0),(1,11,0,0,0),(1,12,0,1,1),(1,13,42,1,0),(1,14,41,0,0),(1,15,44,1,0),(1,19,0,0,0),(1,20,20,1,0),(1,21,59,1,0),(1,22,0,0,0),(1,26,0,0,0),(1,27,0,0,0),(1,28,0,0,0),(1,29,0,0,0),(1,30,0,0,0),(1,31,0,0,0),(1,32,0,0,0),(1,33,0,0,0),(1,34,0,0,0),(1,35,0,0,0),(1,36,0,0,0),(1,37,0,0,0),(1,38,0,0,0),(1,42,0,0,0),(1,54,115,1,0),(1,59,0,0,0),(2,44,94,0,68),(2,45,0,1,0),(2,46,0,0,0),(2,47,0,1,0),(2,48,20,1,90),(4,4,7,1,0),(5,5,8,1,0),(6,6,0,0,0),(7,7,0,0,0),(8,10,27,0,0),(12,23,51,0,0),(12,25,0,1,0),(18,16,48,1,85),(18,17,0,0,0),(18,18,121,1,70),(18,39,72,1,0),(18,40,86,1,0),(18,43,93,1,0),(21,24,52,0,0),(26,41,0,0,0),(27,50,99,1,0),(27,51,100,1,113),(27,56,120,1,0),(28,52,112,0,98),(28,53,109,1,0),(28,55,0,0,0),(32,57,125,1,124),(35,58,127,0,0);
/*!40000 ALTER TABLE `organizationcustomermappings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21  8:50:30
