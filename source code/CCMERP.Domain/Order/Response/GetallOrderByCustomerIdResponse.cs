﻿using CCMERP.Domain.AddItem.Request;
using CCMERP.Domain.Order.Data;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Order.Response
{
    public class GetallOrderByCustomerIdResponse : PaginationResponse
    {
        public List<GetallOrderByCustomerIdData> getallOrderByCustomerIdDatas { get; set; }
    }

    public class GetallOrderItemByHeaderIdResponse 
    {
        public SalesOrderHeaderModel salesOrderHeaderModel{ get; set; }
        public List<SalesOrderDeatils> salesOrderitems { get; set; }
    }
}
