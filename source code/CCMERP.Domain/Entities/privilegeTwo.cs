﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Entities
{
    public class privilegeTwo
    {
        [Key]
        public int privilegeId { get; set; }
        public string privilegeName { get; set; }
        public string privilegeCode { get; set; }
        public DateTime privilegeDate { get; set; }
        public double privilegePrice { get; set; }
        public decimal privilegeAmount { get; set; }
        [NotMapped]
        public string countryName { get; set; }
    }
}
