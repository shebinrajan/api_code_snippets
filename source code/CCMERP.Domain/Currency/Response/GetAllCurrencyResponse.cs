﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Corrency.Response
{
    public class GetAllCurrencyResponse : PaginationResponse
    {

        public List<CurrencyMaster> Currencies { get; set; }
    }
}
