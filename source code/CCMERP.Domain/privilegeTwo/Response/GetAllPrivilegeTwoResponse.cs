﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.PrivilegeOne.Response
{
    public class GetAllPrivilegeTwoResponse : PaginationResponse
    {

        public List<privilegeTwo> PrivilegeTwo { get; set; }
    }
}
