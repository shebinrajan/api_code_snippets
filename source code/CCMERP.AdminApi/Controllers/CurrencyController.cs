﻿using CCMERP.Service.Features.CurrencyService.Commands;
using CCMERP.Service.Features.CurrencyService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V1/Currency")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();


        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllCurrencyQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }


        [HttpGet("{currencyID}")]
        public async Task<IActionResult> GetById(int currencyID)
        {
            return Ok(await Mediator.Send(new GetCurrencyByIdQuery { currencyID = currencyID }));
        }


        [HttpDelete("{currencyID}")]
        public async Task<IActionResult> Delete(int currencyID)
        {
            return Ok(await Mediator.Send(new DeleteCurrencyByIdCommand { currencyID = currencyID }));
        }


        [HttpPost]
        public async Task<IActionResult> Create(CreateCurrencyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }



        [HttpPut]
        public async Task<IActionResult> Update(UpdateCurrencyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}

