﻿using CCMERP.Service.Features.privilegeTwoService.Commands;
using CCMERP.Service.Features.privilegeTwoService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V1/Privileges")]
    [ApiController]
    public class PrivilegeTwoController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();


        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllPrivilegeTwoQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }


        [HttpGet("{privilegeId}")]
        public async Task<IActionResult> GetById(int privilegeId)
        {
            return Ok(await Mediator.Send(new GetPrivilegeTwoByIdQuery { privilegeId = privilegeId }));
        }


        [HttpDelete("{privilegeId}")]
        public async Task<IActionResult> Delete(int privilegeId)
        {
            return Ok(await Mediator.Send(new DeletePrivilegeTwoByIdCommand { privilegeId = privilegeId }));
        }


        [HttpPost]
        public async Task<IActionResult> Create(CreatePrivilegeTwoCommand command)
        {
            return Ok(await Mediator.Send(command));
        }



        [HttpPut]
        public async Task<IActionResult> Update(UpdatePrivilegeTwoCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

    }
}
