﻿using CCMERP.Domain.AddItem.Request;
using CCMERP.Domain.AddItem.Response;
using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.ItemService.Queries
{
    public class GetallItemByIdAutoCompleteQuery : IRequest<Response<GetallItemByIdAutoCompleteResponse>>
    {

        public string itemTitle { get; set; }
        public int orgId { get; set; }
        public string itemCode { get; set; }
        public class GetallItemByIdAutoCompleteQueryHandler : IRequestHandler<GetallItemByIdAutoCompleteQuery, Response<GetallItemByIdAutoCompleteResponse>>
        {
            private readonly TransactionDbContext _context;
            public GetallItemByIdAutoCompleteQueryHandler(TransactionDbContext context)
            {
                _context = context;
            }
            public async Task<Response<GetallItemByIdAutoCompleteResponse>> Handle(GetallItemByIdAutoCompleteQuery request, CancellationToken cancellationToken)
            {

                GetallItemByIdAutoCompleteResponse GetallItemByIdAutoCompleteResponse = new GetallItemByIdAutoCompleteResponse();
                try
                {
                  

                    if (!string.IsNullOrWhiteSpace(request.itemTitle))
                    {
                        if (request.itemTitle.Trim().Length > 2)
                        {
                            GetallItemByIdAutoCompleteResponse.items = _context.itemmaster.Where(a => a.OrgId == request.orgId && a.ItemName.Contains(request.itemTitle)).Select(item1 => new AddItemsRequest
                            {
                                itemid = item1.ItemId,
                                itemCode = item1.ItemCode,
                                itemName = item1.ItemName,
                                uomid = item1.UOMID,
                                uomName = (_context.uOMMaster.Where(b => b.UOMId == item1.UOMID).FirstOrDefault().Name),
                                basePrice = item1.BasePrice,
                                minOrderQty = item1.MinOrderQty,
                                orgid = item1.OrgId
                            }).ToList();
                        }
                        else
                        {

                            return await Task.FromResult(new Response<GetallItemByIdAutoCompleteResponse>(GetallItemByIdAutoCompleteResponse, "Item title length should be three letters", false));
                        }
                    }
                    else
                    {
                        if (request.itemCode.Trim().Length > 2)
                        {
                            GetallItemByIdAutoCompleteResponse.items = _context.itemmaster.Where(a => a.OrgId == request.orgId && a.ItemCode.Contains(request.itemCode)).Select(item1 => new AddItemsRequest
                        {
                            itemid = item1.ItemId,
                            itemCode = item1.ItemCode,
                            itemName = item1.ItemName,
                            uomid = item1.UOMID,
                            uomName = (_context.uOMMaster.Where(b => b.UOMId == item1.UOMID).FirstOrDefault().Name),
                            basePrice = item1.BasePrice,
                            minOrderQty = item1.MinOrderQty,
                            orgid = item1.OrgId
                        }).ToList();
                        }
                        else
                        {

                            return await Task.FromResult(new Response<GetallItemByIdAutoCompleteResponse>(GetallItemByIdAutoCompleteResponse, "Item code length should be three letters", false));
                        }
                    }

                    if (GetallItemByIdAutoCompleteResponse.items.Count==0)
                    {
                        //getAllOrganizationsResponse.totalNoRecords = OrganizationList.Count;
                        return await Task.FromResult(new Response<GetallItemByIdAutoCompleteResponse>(GetallItemByIdAutoCompleteResponse, "No record found ", false));
                    }
                    else
                    {

                        return await Task.FromResult(new Response<GetallItemByIdAutoCompleteResponse>(GetallItemByIdAutoCompleteResponse, "Success", true));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<GetallItemByIdAutoCompleteResponse>(GetallItemByIdAutoCompleteResponse, "Exception", false));
                }


            }
        }

    }
}