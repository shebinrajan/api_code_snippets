﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace CCMERP.Service.Features.privilegeTwoService.Queries
{
    public class GetPrivilegeTwoByIdQuery : IRequest<Response<privilegeTwo>>
    {
        public int privilegeId { get; set; }

        public class GetPrivilegeTwoByIdQueryHandler : IRequestHandler<GetPrivilegeTwoByIdQuery, Response<privilegeTwo>>
        {
            private readonly IdentityContext _context;

            public GetPrivilegeTwoByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<privilegeTwo>> Handle(GetPrivilegeTwoByIdQuery request, CancellationToken cancellationToken)
            {

                privilegeTwo privi = new privilegeTwo();
                try
                {
                    //country =

                    privi = (from t1 in _context.privilege
                             join t2 in _context.country_master on t1.privilegeId equals t2.CountryID
                             where t1.privilegeId == request.privilegeId
                                 select new privilegeTwo
                                 {
                                    privilegeCode = t1.privilegeCode,
                                     privilegeName = t1.privilegeName,
                                     privilegePrice = t1.privilegePrice,
                                     privilegeAmount = t1.privilegeAmount,
                                     countryName = t2.CountryName
                                 }).FirstOrDefault();

                    if (privi != null)
                    {
                        return await Task.FromResult(new Response<privilegeTwo>(privi, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<privilegeTwo>(privi, "No record found ", false));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<privilegeTwo>(privi, "Exception", false));
                }

            }
        }



    }
}
