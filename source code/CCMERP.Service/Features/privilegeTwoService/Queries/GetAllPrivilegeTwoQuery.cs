﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.PrivilegeOne.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.privilegeTwoService.Queries
{
    public class GetAllPrivilegeTwoQuery : IRequest<Response<GetAllPrivilegeTwoResponse>>
    {

        public GetAllPrivilegeTwoQuery()
        {

        }

        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }



        public class GetAllPrivilegeQuery : IRequestHandler<GetAllPrivilegeTwoQuery, Response<GetAllPrivilegeTwoResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllPrivilegeQuery(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllPrivilegeTwoResponse>> Handle(GetAllPrivilegeTwoQuery request, CancellationToken cancellationToken)
            {

                GetAllPrivilegeTwoResponse getAllPrivilegeTwoResponse = new GetAllPrivilegeTwoResponse();
                try
                {
                    var PrivilegeTwoList = await
                        (
                        from o in _context.privilege

                        select new privilegeTwo()
                        {
                            privilegeId = o.privilegeId,
                            privilegeCode = o.privilegeCode,
                            privilegeName = o.privilegeName,
                            privilegeDate = DateTime.Now,
                            privilegePrice = o.privilegePrice,
                            privilegeAmount = o.privilegeAmount,

                        }).Distinct().ToListAsync();
                    if (PrivilegeTwoList.Count == 0)
                    {

                        return new Response<GetAllPrivilegeTwoResponse>(getAllPrivilegeTwoResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllPrivilegeTwoResponse.TotalItems = PrivilegeTwoList.Count;
                        getAllPrivilegeTwoResponse.TotalPages = (int)Math.Ceiling(getAllPrivilegeTwoResponse.TotalItems / (double)request.PageSize);
                        getAllPrivilegeTwoResponse.PrivilegeTwo = PrivilegeTwoList.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllPrivilegeTwoResponse>(getAllPrivilegeTwoResponse, "Success", true);
                    }
                }
                catch (Exception)
                {

                    return new Response<GetAllPrivilegeTwoResponse>(getAllPrivilegeTwoResponse, "Exception", false);
                }


            }
        }


    }
}
