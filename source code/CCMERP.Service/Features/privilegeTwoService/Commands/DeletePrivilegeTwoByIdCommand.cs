﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.privilegeTwoService.Commands
{
    public class DeletePrivilegeTwoByIdCommand : IRequest<Response<int>>
    {
        [Required]
        public int privilegeId { get; set; }

        public class DeletePrivilegeTwoByIdCommandHandler : IRequestHandler<DeletePrivilegeTwoByIdCommand, Response<int>>
        {

            private readonly IdentityContext _context;
            public DeletePrivilegeTwoByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeletePrivilegeTwoByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var privilegeTwo = await _context.privilege.FindAsync(request.privilegeId);
                    if (privilegeTwo == null)
                    {
                        return new Response<int>(0, "No Privilege found ", false);
                    }
                    else
                    {
                        var deletePrivilegeTwoId = request.privilegeId;
                        _context.Remove(privilegeTwo);
                        await _context.SaveChangesAsync();

                        return new Response<int>(deletePrivilegeTwoId, "Privilege deleted successfully", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}
