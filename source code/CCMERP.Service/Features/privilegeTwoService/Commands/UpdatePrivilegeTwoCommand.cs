﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.privilegeTwoService.Commands
{
    public class UpdatePrivilegeTwoCommand : IRequest<Response<int>>
    {
        [Required]
        public int privilegeId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string privilegeName { get; set; }
        [Required]
        public string privilegeCode { get; set; }
        [Required]
        public double privilegePrice { get; set; }
        [Required]
        public long privilegeAmount { get; set; }

        public class UpdatePrivilegeTwoCommandHandler : IRequestHandler<UpdatePrivilegeTwoCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdatePrivilegeTwoCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdatePrivilegeTwoCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var pri = _context.privilege.Where(a => a.privilegeId == request.privilegeId).FirstOrDefault();

                    if (pri == null)
                    {
                        return new Response<int>(0, "No Privilege found", true);
                    }
                    else
                    {
                        pri.privilegeCode = request.privilegeCode;
                        pri.privilegeName = request.privilegeName;
                        pri.privilegeDate = DateTime.Now;
                        pri.privilegePrice = request.privilegePrice;
                        pri.privilegeAmount = request.privilegeAmount;

                        _context.privilege.Update(pri);
                        await _context.SaveChangesAsync();

                        return new Response<int>(pri.privilegeId, "Success", true);
                    }
                }
                catch (Exception ex)
                {   
                    return new Response<int>(0, "Exception", false);
                }
            }
        }
    }
}
