﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.privilegeTwoService.Commands
{
    public class CreatePrivilegeTwoCommand : IRequest<Response<int>>
    {
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string privilegeName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string privilegeCode { get; set; }
        [Required]
        public double privilegePrice { get; set; }
        [Required]
        public decimal privilegeAmount { get; set; }



        public class CreatePrivilegeTwoCommandHandler : IRequestHandler<CreatePrivilegeTwoCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            //private readonly ITransactionDbContext _tcontext;
            public CreatePrivilegeTwoCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreatePrivilegeTwoCommand request, CancellationToken cancellationToken)
            {

                try
                {

                    var cou = _context.privilege.Where(a => a.privilegeName.ToLower() == request.privilegeName.ToLower()).ToList();
                    if (cou.Count > 0)
                    {
                        return new Response<int>(0, "An Privilege with the same name already exists", false);
                    }
                    else
                    {

                        privilegeTwo privilegeTwo = new privilegeTwo()
                        {
                            privilegeCode = request.privilegeCode,
                            privilegeName = request.privilegeName,
                            privilegeDate = DateTime.Now,
                            privilegePrice = request.privilegePrice,
                            privilegeAmount = request.privilegeAmount
                        };


                        _context.privilege.Add(privilegeTwo);
                        await _context.SaveChangesAsync();

                        return new Response<int>(privilegeTwo.privilegeId, "Success", true);
                    }


                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}
