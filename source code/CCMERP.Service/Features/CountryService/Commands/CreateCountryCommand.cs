﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CountryService.Commands
{
    public class CreateCountryCommand : IRequest<Response<int>>
    {
       
      
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CountryName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string ABR { get; set; }
        [Required]
        public int IsActive { get; set; }
        [Required]        
        public int CountryCode { get; set; }


        public class CreateCountryCommandHandler : IRequestHandler<CreateCountryCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            //private readonly ITransactionDbContext _tcontext;
            public CreateCountryCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreateCountryCommand request, CancellationToken cancellationToken)
            {

                try
                {

                    var cou = _context.country_master.Where(a => a.CountryName.ToLower() == request.CountryName.ToLower()).ToList();
                    if (cou.Count > 0)
                    {
                        return new Response<int>(0, "An country with the same name already exists", false);
                    }
                    else
                    {

                        CountryMaster country = new CountryMaster()
                        {
                            ABR = request.ABR,
                            CountryCode = request.CountryCode,                           
                            CountryName= request.CountryName,
                            IsActive= request.IsActive
                            
                        };


                        _context.country_master.Add(country);
                        await _context.SaveChangesAsync();
                       
                        return new Response<int>(country.CountryID, "Success", true);
                    }


                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }

                
            }
        }



    }
}
