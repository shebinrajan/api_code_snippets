﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CountryService.Commands
{
   public class DeleteCountryByIdCommand : IRequest<Response<int>>
    {

        [Required]      
        public int countryId { get; set; }

        public class DeleteCountryByIdCommandHandler : IRequestHandler<DeleteCountryByIdCommand, Response<int>>
        {
            
            private readonly IdentityContext _context;
            public DeleteCountryByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeleteCountryByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var country = await _context.country_master.FindAsync(request.countryId);
                    if (country == null)
                    {
                        return new Response<int>(0, "No country found ", false);
                    }
                    else
                    {
                        var deleteCountryId = request.countryId;
                        country.IsActive = 0;
                        _context.Remove(country);
                        await _context.SaveChangesAsync();                      

                        return new Response<int>(deleteCountryId, "country deleted successfully", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}
