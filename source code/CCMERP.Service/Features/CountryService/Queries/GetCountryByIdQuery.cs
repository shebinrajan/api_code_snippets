﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace CCMERP.Service.Features.CountryService.Queries
{
    public class GetCountryByIdQuery : IRequest<Response<CountryMaster>>
    {
        public int countryId { get; set; }

        public class GetCountryByIdQueryHandler : IRequestHandler<GetCountryByIdQuery, Response<CountryMaster>>
        {
            private readonly IdentityContext _context;

            public GetCountryByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<CountryMaster>> Handle(GetCountryByIdQuery request, CancellationToken cancellationToken)
            {

                CountryMaster country = new CountryMaster();
                try
                {
                    //country =

                    country = (from t1 in _context.country_master
                                   
                                    where t1.CountryID == request.countryId
                               select new CountryMaster
                                    {
                                        ABR = t1.ABR,
                                        CountryCode = t1.CountryCode,
                                        CountryName = t1.CountryName,
                                        IsActive = t1.IsActive,
                                        CountryID=t1.CountryID
                                       

                                    }).FirstOrDefault();

                    if (country != null)
                    {
                        return await Task.FromResult(new Response<CountryMaster>(country, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<CountryMaster>(country, "No record found ", false));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<CountryMaster>(country, "Exception", false));
                }

            }
        }



    }
}
