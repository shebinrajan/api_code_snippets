﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CCMERP.Domain.Corrency.Response;

namespace CCMERP.Service.Features.CurrencyService.Queries
{
    public class GetAllCurrencyQuery : IRequest<Response<GetAllCurrencyResponse>>
    {

        public GetAllCurrencyQuery()
        {

        }

        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }



        public class GetAllCurrencyOneQuery : IRequestHandler<GetAllCurrencyQuery, Response<GetAllCurrencyResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllCurrencyOneQuery(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllCurrencyResponse>> Handle(GetAllCurrencyQuery request, CancellationToken cancellationToken)
            {

                GetAllCurrencyResponse getAllCurrencyResponse = new GetAllCurrencyResponse();
                try
                {
                    var CurrencyList = await
                        (
                        from o in _context.currency_master

                        select new CurrencyMaster()
                        {
                            CurrencyABR = o.CurrencyABR,
                            CurrencyID = o.CurrencyID,
                            CountryID=o.CountryID,
                            CurrencyName = o.CurrencyName,

                           
                        }).Distinct().ToListAsync();
                    if (CurrencyList.Count == 0)
                    {

                        return new Response<GetAllCurrencyResponse>(getAllCurrencyResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllCurrencyResponse.TotalItems = CurrencyList.Count;
                        getAllCurrencyResponse.TotalPages = (int)Math.Ceiling(getAllCurrencyResponse.TotalItems / (double)request.PageSize);
                        getAllCurrencyResponse.Currencies = CurrencyList. Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllCurrencyResponse>(getAllCurrencyResponse, "Success", true);
                    }
                }
                catch (Exception)
                {

                    return new Response<GetAllCurrencyResponse>(getAllCurrencyResponse, "Exception", false);
                }


            }
        }


    }
}
