﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Corrency.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace CCMERP.Service.Features.CurrencyService.Queries
{
    public class GetCurrencyByIdQuery : IRequest<Response<CurrencyMaster>>
    {
        public int currencyID { get; set; }

        public class GetCurrencyByIdQueryHandler : IRequestHandler<GetCurrencyByIdQuery, Response<CurrencyMaster>>
        {
            private readonly IdentityContext _context;

            public GetCurrencyByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<CurrencyMaster>> Handle(GetCurrencyByIdQuery request, CancellationToken cancellationToken)
            {

                CurrencyMaster currency = new CurrencyMaster();
                try
                {
                    //country =

                    currency = (from t1 in _context.currency_master

                                where t1.CurrencyID == request.currencyID
                                select new CurrencyMaster
                                    {
                                        CurrencyABR = t1.CurrencyABR,
                                        CurrencyID = t1.CurrencyID,
                                        CurrencyName = t1.CurrencyName,
                                        CountryID=t1.CountryID
                                       

                                    }).FirstOrDefault();

                    if (currency != null)
                    {
                        return await Task.FromResult(new Response<CurrencyMaster>(currency, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<CurrencyMaster>(currency, "No record found ", false));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<CurrencyMaster>(currency, "Exception", false));
                }

            }
        }



    }
}
