﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CurrencyService.Commands
{
    public class CreateCurrencyCommand : IRequest<Response<int>>
    {
      
       
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CurrencyName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CurrencyABR { get; set; }
        [Required]        
        public int CountryID { get; set; }


        public class CreateCurrencyCommandHandler : IRequestHandler<CreateCurrencyCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            //private readonly ITransactionDbContext _tcontext;
            public CreateCurrencyCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreateCurrencyCommand request, CancellationToken cancellationToken)
            {

                try
                {

                    var cur = _context.currency_master.Where(a => a.CurrencyName.ToLower() == request.CurrencyName.ToLower()).ToList();
                    if (cur.Count > 0)
                    {
                        return new Response<int>(0, "An entry with the same name already exists", false);
                    }
                    else
                    {

                        CurrencyMaster currency = new CurrencyMaster()
                        {
                            CurrencyABR = request.CurrencyABR,
                            CountryID= request.CountryID,
                            CurrencyName = request.CurrencyName,
                            
                        };


                        _context.currency_master.Add(currency);
                        await _context.SaveChangesAsync();
                       
                        return new Response<int>(currency.CurrencyID, "Success", true);
                    }


                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }

                
            }
        }



    }
}
