﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CurrencyService.Commands
{
    public class UpdateCurrencyCommand : IRequest<Response<int>>
    {
        [Required]
        public int CurrencyID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CurrencyName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CurrencyABR { get; set; }
        [Required]
        public int CountryID { get; set; }


        public class UpdateCurrencyCommandHandler : IRequestHandler<UpdateCurrencyCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdateCurrencyCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdateCurrencyCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var currency = _context.currency_master.Where(a => a.CurrencyID == request.CurrencyID).FirstOrDefault();

                    if (currency == null)
                    {
                        return new Response<int>(0, "No currency found", true);
                    }
                    else
                    {

                        currency.CurrencyABR = request.CurrencyABR;
                        currency.CountryID = request.CountryID;
                        currency.CurrencyName = request.CurrencyName;


                        _context.currency_master.Update(currency);
                        await _context.SaveChangesAsync();

                        return new Response<int>(currency.CurrencyID, "Success", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}
