﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CurrencyService.Commands
{
   public class DeleteCurrencyByIdCommand : IRequest<Response<int>>
    {

        [Required]      
        public int currencyID { get; set; }

        public class DeleteCurrencyByIdCommandHandler : IRequestHandler<DeleteCurrencyByIdCommand, Response<int>>
        {
            
            private readonly IdentityContext _context;
            public DeleteCurrencyByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeleteCurrencyByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var currency = await _context.currency_master.FindAsync(request.currencyID);
                    if (currency == null)
                    {
                        return new Response<int>(0, "No currency found ", false);
                    }
                    else
                    {
                        var deleteCurrencyId = request.currencyID;
                        _context.Remove(currency);
                        await _context.SaveChangesAsync();                      

                        return new Response<int>(deleteCurrencyId, "Currency deleted successfully", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}
