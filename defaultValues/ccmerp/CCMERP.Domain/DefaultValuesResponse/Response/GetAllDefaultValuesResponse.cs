﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.DefaultValuesResponse.Response
{
    public class GetAllDefaultValuesResponse : PaginationResponse
    {
        public GetAllDefaultValuesResponse() : base()
        {

        }
        public List<DefaultValues> DefaultValues { get; set; }

    }
}

