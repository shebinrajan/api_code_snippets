﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Customers.Response
{
    public class GetAllCustomersResponse: PaginationResponse
    {
        public GetAllCustomersResponse():base()
        {
                
        }
        public List<Customer> customers { get; set; }
       
    }
}
