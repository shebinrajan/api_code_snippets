﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CCMERP.Domain.Entities
{
    public class DefaultValues
    {
        [Key]
        public int DefaultID { get; set; }
        public string DefaultName { get; set; }
        public decimal Defaultvalue1 { get; set; }
        public DateTime Defaultvalue2 { get; set; }
        public DateTime Defaultvalue3 { get; set; }
        public float Defaultvalue4 { get; set; }
        public double Defaultvalue5 { get; set; }
        public bool Defaultvalue6 { get; set; }
        public string Defaultvalue7 { get; set; }
        public string Defaultvalue8 { get; set; }
        public string Defaultvalue9 { get; set; }
        public Int16 Defaultvalue10 { get; set; }
        public Int32 Defaultvalue11 { get; set; }
        public Int64 Defaultvalue12 { get; set; }
        public int Defaultvalue13 { get; set; }
        public long Defaultvalue14 { get; set; }
        public short Defaultvalue15 { get; set; }
        public string Defaultvalue16 { get; set; }
        public string Defaultvalue17 { get; set; }
    }
}
