﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Order.Data
{
   public class GetallOrderByCustomerIdData
    {
		public int sohdrId { get; set; }
		public int orgId { get; set; }
		public int customerId { get; set; }
		public string soNo { get; set; }
		public string soDate { get; set; }
		public string expectedDate { get; set; }
		public string status { get; set; }
	}
}
