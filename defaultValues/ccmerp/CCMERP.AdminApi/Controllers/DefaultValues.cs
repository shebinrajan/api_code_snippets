﻿using CCMERP.Service.Features.DefaultValue.Commands;
using CCMERP.Service.Features.DefaultValue.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;


namespace CCMERP.AdminApi.Controllers
{
    [ApiController]
    [Route("api/v2/DefaultValues")]
    public class DefaultValues : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        [HttpPost]
        public async Task<IActionResult> Create(CreateDefaultValuesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllDefaultValuesQuery { _pageSize = pageSize, PageNumber = pageNumber }));
        }

        [HttpGet("{DefaultID}")]
        public async Task<IActionResult> GetById(int DefaultID)
        {
            return Ok(await Mediator.Send(new GetDefaultValuesByIdQuery { DefaultID = DefaultID }));
        }

        [HttpDelete("{DefaultID}")]
        public async Task<IActionResult> Delete(int DefaultID)
        {
            return Ok(await Mediator.Send(new DeleteDefaultValuesByIdCommand { DefaultID = DefaultID }));
        }


        [HttpPut]
        public async Task<IActionResult> Update(UpdateDefaultValuesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
