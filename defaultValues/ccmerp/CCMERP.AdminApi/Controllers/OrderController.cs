﻿using CCMERP.Service.Features.Order.Commands;
using CCMERP.Service.Features.Order.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;


namespace CCMERP.AdminApi.Controllers
{
    [Route("api/v1/Order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        [HttpPost("CreateOrder")]
        public async Task<IActionResult> AddItem([FromBody] CreateOrderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetallOrderByCustomerId")]
        public async Task<IActionResult> GetallOrderByCustomerId(int orgId, int customerId, int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetallOrderByCustomerIdQuery { orgId = orgId,customerId= customerId, _pageSize = pageSize, PageNumber = pageNumber }));
        }


        //[HttpGet("GetallOrderItemByHeaderId")]
        //public async Task<IActionResult> GetallOrderItemByHeaderId(int orgId, int headerId, int pageNumber = 1, int pageSize = 25)
        //{
        //    return Ok(await Mediator.Send(new GetallOrderItemByHeaderIdQuery { orgId = orgId, headerId = headerId, _pageSize = pageSize, PageNumber = pageNumber }));
        //}
    }
}
