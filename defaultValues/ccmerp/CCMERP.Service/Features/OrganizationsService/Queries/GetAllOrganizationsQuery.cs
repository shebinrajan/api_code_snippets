﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Organizations.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.OrganizationsService.Queries
{
    public class GetAllOrganizationsQuery : IRequest<Response<GetAllOrganizationsResponse>>
    {
        public GetAllOrganizationsQuery()
        {
                
        }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public class GetAllCurrencyQueryHandler : IRequestHandler<GetAllOrganizationsQuery, Response<GetAllOrganizationsResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllCurrencyQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllOrganizationsResponse>> Handle(GetAllOrganizationsQuery request, CancellationToken cancellationToken)
            {

                GetAllOrganizationsResponse getAllOrganizationsResponse = new GetAllOrganizationsResponse();
                try
                {
                    var OrganizationList = await  
                        (
                        from o in _context.Organization
                       
                        select new Organization()
                        {
                            Org_ID = o.Org_ID,
                    Name = o.Name,
                    Address1 = o.Address1,
                    Address2 = o.Address2,
                    City = o.City,
                    State = o.State,
                    CountryID = o.CountryID,
                    Currency_ID = o.Currency_ID,
                    ContactPerson = o.ContactPerson,
                    ContactNumber = o.ContactNumber,
                    VATID = o.VATID,
                    TaxWording = o.TaxWording,
                    LastModifiedBy = o.LastModifiedBy,
                    LastModifiedDate = DateTime.Now,
                    LastModifiedByProgram = o.LastModifiedByProgram,
                    Zipcode = o.Zipcode,
                    IsActive=o.IsActive
                }).Distinct().ToListAsync();
                    if (OrganizationList.Count == 0)
                    {
                        
                        return new Response<GetAllOrganizationsResponse>(getAllOrganizationsResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllOrganizationsResponse.TotalItems = OrganizationList.Count;
                        getAllOrganizationsResponse.TotalPages = (int)Math.Ceiling(getAllOrganizationsResponse.TotalItems / (double)request.PageSize);
                        getAllOrganizationsResponse.organizations = OrganizationList.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllOrganizationsResponse>(getAllOrganizationsResponse, "Success", true);
                    }
                }
                catch (Exception)
                {

                    return new Response<GetAllOrganizationsResponse>(getAllOrganizationsResponse, "Exception", false);
                }


            }
        }



    }
}
