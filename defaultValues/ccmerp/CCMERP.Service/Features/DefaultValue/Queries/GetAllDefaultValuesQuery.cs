﻿using CCMERP.Domain.Common;
using CCMERP.Domain.DefaultValuesResponse.Response;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace CCMERP.Service.Features.DefaultValue.Queries
{
    public class GetAllDefaultValuesQuery : IRequest<Response<GetAllDefaultValuesResponse>>
    {

        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public class GetAllDefaultValuesQueryHandler : IRequestHandler<GetAllDefaultValuesQuery, Response<GetAllDefaultValuesResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllDefaultValuesQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllDefaultValuesResponse>> Handle(GetAllDefaultValuesQuery request, CancellationToken cancellationToken)
            {

                GetAllDefaultValuesResponse defaultValuesResponse = new GetAllDefaultValuesResponse();
                List<DefaultValues> defaultValues = new List<DefaultValues>();
                try
                {
                   
                        defaultValues = await (from c in _context.defaultvalues
                                               select new DefaultValues()
                                           {
                                                   DefaultID = c.DefaultID,
                                                   DefaultName = c.DefaultName,
                                                   Defaultvalue1 = c.Defaultvalue1,
                                                   Defaultvalue2 = c.Defaultvalue2,
                                                   Defaultvalue3 = c.Defaultvalue3,
                                                   Defaultvalue4 = c.Defaultvalue4,
                                                   Defaultvalue5 = c.Defaultvalue5,
                                                   Defaultvalue6 = c.Defaultvalue6,
                                                   Defaultvalue7 = c.Defaultvalue7,
                                                   Defaultvalue8 = c.Defaultvalue8,
                                                   Defaultvalue9 = c.Defaultvalue9,
                                                   Defaultvalue10 = c.Defaultvalue10,
                                                   Defaultvalue11 = c.Defaultvalue11,
                                                   Defaultvalue12 = c.Defaultvalue12,
                                                   Defaultvalue13 = c.Defaultvalue13,
                                                   Defaultvalue14 = c.Defaultvalue14,
                                                   Defaultvalue15 = c.Defaultvalue15,
                                                   Defaultvalue16 = c.Defaultvalue16,
                                                   Defaultvalue17 = c.Defaultvalue17
                                               }).ToListAsync();

                    if (defaultValues.Count == 0)
                    {
                        
                        return new Response<GetAllDefaultValuesResponse>(defaultValuesResponse, "No record found ", false);
                    }
                    else
                    {
                        defaultValuesResponse.TotalItems = defaultValues.Count;
                        defaultValuesResponse.TotalPages = (int)Math.Ceiling(defaultValuesResponse.TotalItems / (double)request.PageSize);
                        defaultValuesResponse.DefaultValues = defaultValues.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllDefaultValuesResponse>(defaultValuesResponse, "Success", true);
                    }
                }
                catch (Exception ex)
                {

                    return new Response<GetAllDefaultValuesResponse>(defaultValuesResponse, "Exception", false);
                }


            }
        }
    }
}
