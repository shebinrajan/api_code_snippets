﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValue.Queries
{
    public class GetDefaultValuesByIdQuery : IRequest<Response<DefaultValues>>
    {
        public int DefaultID { get; set; }
        public class GetDefaultValuesByIdQueryHandler : IRequestHandler<GetDefaultValuesByIdQuery, Response<DefaultValues>>
        {
            private readonly IdentityContext _context;
            public GetDefaultValuesByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<DefaultValues>> Handle(GetDefaultValuesByIdQuery request, CancellationToken cancellationToken)
            {

                DefaultValues defaultValues = new DefaultValues();
                try
                {
                    var dafault = _context.defaultvalues.ToList();


                    defaultValues = (from c in dafault
                                where c.DefaultID == request.DefaultID
                                     select new DefaultValues
                                     {
                                         DefaultID = c.DefaultID,
                                         DefaultName = c.DefaultName,
                                         Defaultvalue1 = c.Defaultvalue1,
                                         Defaultvalue2 = c.Defaultvalue2,
                                         Defaultvalue3 = c.Defaultvalue3,
                                         Defaultvalue4 = c.Defaultvalue4,
                                         Defaultvalue5 = c.Defaultvalue5,
                                         Defaultvalue6 = c.Defaultvalue6,
                                         Defaultvalue7 = c.Defaultvalue7,
                                         Defaultvalue8 = c.Defaultvalue8,
                                         Defaultvalue9 = c.Defaultvalue9,
                                         Defaultvalue10 = c.Defaultvalue10,
                                         Defaultvalue11 = c.Defaultvalue11,
                                         Defaultvalue12 = c.Defaultvalue12,
                                         Defaultvalue13 = c.Defaultvalue13,
                                         Defaultvalue14 = c.Defaultvalue14,
                                         Defaultvalue15 = c.Defaultvalue15,
                                         Defaultvalue16 = c.Defaultvalue16,
                                         Defaultvalue17 = c.Defaultvalue17
                                     }).FirstOrDefault();


                    if (defaultValues != null)
                    {
                        return await Task.FromResult(new Response<DefaultValues>(defaultValues, "Success", true));

                    }
                    else
                    {
                        return await Task.FromResult(new Response<DefaultValues>(defaultValues, "No record found ", false));

                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<DefaultValues>(defaultValues, "Exception", false));
                }


            }
        }
    }
}
