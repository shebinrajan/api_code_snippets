﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValue.Commands
{
    public class DeleteDefaultValuesByIdCommand : IRequest<Response<int>>
    {
        [Required]
        public int DefaultID { get; set; }

        public class DeleteDefaultValuesByIdCommandHandler : IRequestHandler<DeleteDefaultValuesByIdCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public DeleteDefaultValuesByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeleteDefaultValuesByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var item1 = _context.defaultvalues.Where(a => a.DefaultID == request.DefaultID ).FirstOrDefault();
                    if (item1 != null)
                    {
                        _context.defaultvalues.Remove(item1);
                        await _context.SaveChangesAsync();
                        return new Response<int>(1, "Success", true);
                    }
                    else
                    {
                        return new Response<int>(0, "No item found", false);

                    }
                }
                catch (Exception ex)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }
    }
}
