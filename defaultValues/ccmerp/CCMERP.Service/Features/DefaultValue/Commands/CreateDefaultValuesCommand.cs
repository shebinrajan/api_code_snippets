﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValue.Commands
{
    public class CreateDefaultValuesCommand : IRequest<Response<int>>
	{
		[Required]
		public string DefaultName { get; set; }
		public decimal Defaultvalue1 { get; set; }
        public DateTime Defaultvalue2 { get; set; }
        public DateTime Defaultvalue3 { get; set; }
        public float Defaultvalue4 { get; set; }
        public double Defaultvalue5 { get; set; }
        public bool Defaultvalue6 { get; set; }
        public string Defaultvalue7 { get; set; }
        public string Defaultvalue8 { get; set; }
        public string Defaultvalue9 { get; set; }
        public Int16 Defaultvalue10 { get; set; }
        public Int32 Defaultvalue11 { get; set; }
        public Int64 Defaultvalue12 { get; set; }
        public int Defaultvalue13 { get; set; }
        public long Defaultvalue14 { get; set; }
        public short Defaultvalue15 { get; set; }
        public string Defaultvalue16 { get; set; }
        public string Defaultvalue17 { get; set; }

        public class CreateDefaultValuesCommandHandler : IRequestHandler<CreateDefaultValuesCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public CreateDefaultValuesCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(CreateDefaultValuesCommand request, CancellationToken cancellationToken)
            {
                try
                {

				var DefaultValues = new DefaultValues()
				{
					DefaultName = request.DefaultName,
					Defaultvalue1 = request.Defaultvalue1,
                    Defaultvalue2 = DateTime.Now,
                    Defaultvalue3 = DateTime.Now,
                    Defaultvalue4 = request.Defaultvalue4,
                    Defaultvalue5 = request.Defaultvalue5,
                    Defaultvalue6 = request.Defaultvalue6,
                    Defaultvalue7 = request.Defaultvalue7,
                    Defaultvalue8 = request.Defaultvalue8,
                    Defaultvalue9 = request.Defaultvalue9,
                    Defaultvalue10 = request.Defaultvalue10,
                    Defaultvalue11 = request.Defaultvalue11,
                    Defaultvalue12 = request.Defaultvalue12,
                    Defaultvalue13 = request.Defaultvalue13,
                    Defaultvalue14 = request.Defaultvalue14,
                    Defaultvalue15 = request.Defaultvalue15,
                    Defaultvalue16 = request.Defaultvalue16,
                    Defaultvalue17 = request.Defaultvalue17
                };
                _context.defaultvalues.Add(DefaultValues);
                await _context.SaveChangesAsync();

					

					return new Response<int>(DefaultValues.DefaultID, "Success", true);
				}
				catch (Exception ex)
				{
					return new Response<int>(0, "Exception", false);
				}
			}
        }
    }
}
