﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValue.Commands
{
    public class UpdateDefaultValuesCommand : IRequest<Response<int>>
	{
		[Required]
        public int DefaultID { get; set; }
        public string DefaultName { get; set; }
        public decimal Defaultvalue1 { get; set; }
        public DateTime Defaultvalue2 { get; set; }
        public DateTime Defaultvalue3 { get; set; }
        public float Defaultvalue4 { get; set; }
        public double Defaultvalue5 { get; set; }
        public bool Defaultvalue6 { get; set; }
        public string Defaultvalue7 { get; set; }
        public string Defaultvalue8 { get; set; }
        public string Defaultvalue9 { get; set; }
        public Int16 Defaultvalue10 { get; set; }
        public Int32 Defaultvalue11 { get; set; }
        public Int64 Defaultvalue12 { get; set; }
        public int Defaultvalue13 { get; set; }
        public long Defaultvalue14 { get; set; }
        public short Defaultvalue15 { get; set; }
        public string Defaultvalue16 { get; set; }
        public string Defaultvalue17 { get; set; }

        public class UpdateDefaultValuesCommandHandler : IRequestHandler<UpdateDefaultValuesCommand, Response<int>>
        {
			private readonly IdentityContext _context;
			public UpdateDefaultValuesCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdateDefaultValuesCommand request, CancellationToken cancellationToken)
            {
                try
                {

                
                var cust = _context.defaultvalues.Where(a => a.DefaultID == request.DefaultID).FirstOrDefault();

                if (cust == null)
                {
                        return new Response<int>(0, "No customer found", true);
                    }
                else
                {
						cust.DefaultName = request.DefaultName;
						cust.Defaultvalue1 = request.Defaultvalue1;
                        cust.Defaultvalue2 = DateTime.Now;
                        cust.Defaultvalue3 = DateTime.Now;
                        cust.Defaultvalue4 = request.Defaultvalue4;
                        cust.Defaultvalue5 = request.Defaultvalue5;
                        cust.Defaultvalue6 = request.Defaultvalue6;
                        cust.Defaultvalue7 = request.Defaultvalue7;
                        cust.Defaultvalue8 = request.Defaultvalue8;
                        cust.Defaultvalue9 = request.Defaultvalue9;
                        cust.Defaultvalue10 = request.Defaultvalue10;
                        cust.Defaultvalue11 = request.Defaultvalue11;
                        cust.Defaultvalue12 = request.Defaultvalue12; 
                        cust.Defaultvalue13 = request.Defaultvalue13;
                        cust.Defaultvalue14 = request.Defaultvalue14;
                        cust.Defaultvalue15 = request.Defaultvalue15;
                        cust.Defaultvalue16 = request.Defaultvalue16;
                        cust.Defaultvalue17 = request.Defaultvalue17;
                        _context.defaultvalues.Update(cust);
                    await _context.SaveChangesAsync();


                        return new Response<int>(cust.DefaultID, "Success", true);
                    }
                }
                catch (Exception)
                {

					return new Response<int>(0, "Exception", false);
				}
            }
        }
    }
}
