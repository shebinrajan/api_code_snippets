﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Order.Data;
using CCMERP.Domain.Order.Response;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.Order.Queries
{
    public class GetallOrderByCustomerIdQuery : IRequest<Response<GetallOrderByCustomerIdResponse>>
    {
        public int orgId { get; set; }
        public int customerId { get; set; }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
        public class GetallOrderByCustomerIdQueryHandler : IRequestHandler<GetallOrderByCustomerIdQuery, Response<GetallOrderByCustomerIdResponse>>
        {
            private readonly TransactionDbContext _context;
            public GetallOrderByCustomerIdQueryHandler(TransactionDbContext context)
            {
                _context = context;
            }
            public async Task<Response<GetallOrderByCustomerIdResponse>> Handle(GetallOrderByCustomerIdQuery request, CancellationToken cancellationToken)
            {

                GetallOrderByCustomerIdResponse GetallItemByIdResponse = new GetallOrderByCustomerIdResponse();
                try
                {

                    GetallItemByIdResponse.getallOrderByCustomerIdDatas = _context.salesorderheader.Where(a => a.OrgId == request.orgId && a.CustomerId == request.customerId).Select(a => new GetallOrderByCustomerIdData {
                    orgId=request.orgId,
                    customerId=request.customerId,
                    sohdrId=a.SOHdrId,
                    soNo=a.SONo,
                    soDate=a.SODate.ToString("dd-MM-yyyy"),
                    expectedDate=a.ExpectedDate.ToString("dd-MM-yyyy"),
                        status = getSalesorderSataus(a.StatusId)
                    }).ToList();

                    if (GetallItemByIdResponse.getallOrderByCustomerIdDatas.Count == 0)
                    {
                        //getAllOrganizationsResponse.totalNoRecords = OrganizationList.Count;
                        return await Task.FromResult(new Response<GetallOrderByCustomerIdResponse>(GetallItemByIdResponse, "No record found ", false));
                    }
                    else
                    {
                        GetallItemByIdResponse.TotalItems = GetallItemByIdResponse.getallOrderByCustomerIdDatas.Count;
                        GetallItemByIdResponse.TotalPages = (int)Math.Ceiling(GetallItemByIdResponse.TotalItems / (double)request.PageSize);
                        GetallItemByIdResponse.getallOrderByCustomerIdDatas = GetallItemByIdResponse.getallOrderByCustomerIdDatas.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();

                        return await Task.FromResult(new Response<GetallOrderByCustomerIdResponse>(GetallItemByIdResponse, "Success", true));
                    }
                }
                catch (Exception ex)
                {
                    return await Task.FromResult(new Response<GetallOrderByCustomerIdResponse>(GetallItemByIdResponse, "Exception", false));
                }


            }
        }
        public static string getSalesorderSataus(int statusId)
        {
            string status = string.Empty;

            try
            {
                switch (statusId)
                {
                    case 1:
                        status = "Created";
                        break;
                    case 2:
                        status = "Updated";
                        break;
                    //case 3:
                    //    Console.WriteLine("Wednesday");
                    //    break;
                    //case 4:
                    //    Console.WriteLine("Thursday");
                    //    break;
                    //case 5:
                    //    Console.WriteLine("Friday");
                    //    break;
                    //case 6:
                    //    Console.WriteLine("Saturday");
                    //    break;
                    //case 7:
                    //    Console.WriteLine("Sunday");
                    //    break;
                }

            }
            catch (Exception)
            {

                
            }
            return status;
        }
    }
}