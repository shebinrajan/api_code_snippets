﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Order.Data;
using CCMERP.Domain.Order.Response;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.Order.Queries
{
    public class GetallOrderItemByHeaderIdQuery : IRequest<Response<GetallOrderItemByHeaderIdResponse>>
    {
        public int orgId { get; set; }
        public int headerId { get; set; }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
        public class GetallOrderItemByHeaderIdQueryHandler : IRequestHandler<GetallOrderItemByHeaderIdQuery, Response<GetallOrderItemByHeaderIdResponse>>
        {
            private readonly TransactionDbContext _context;
            public GetallOrderItemByHeaderIdQueryHandler(TransactionDbContext context)
            {
                _context = context;
            }
            public async Task<Response<GetallOrderItemByHeaderIdResponse>> Handle(GetallOrderItemByHeaderIdQuery request, CancellationToken cancellationToken)
            {

                GetallOrderItemByHeaderIdResponse GetallItemByIdResponse = new GetallOrderItemByHeaderIdResponse();
                try
                {
                    if (GetallItemByIdResponse.getallOrderByCustomerIdDatas.Count == 0)
                    {
                       
                        return await Task.FromResult(new Response<GetallOrderItemByHeaderIdResponse>(GetallItemByIdResponse, "No record found ", false));
                    }
                    else
                    {
                        GetallItemByIdResponse.TotalItems = GetallItemByIdResponse.getallOrderByCustomerIdDatas.Count;
                        GetallItemByIdResponse.TotalPages = (int)Math.Ceiling(GetallItemByIdResponse.TotalItems / (double)request.PageSize);
                        GetallItemByIdResponse.getallOrderByCustomerIdDatas = GetallItemByIdResponse.getallOrderByCustomerIdDatas.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();

                        return await Task.FromResult(new Response<GetallOrderItemByHeaderIdResponse>(GetallItemByIdResponse, "Success", true));
                    }
                }
                catch (Exception ex)
                {
                    return await Task.FromResult(new Response<GetallOrderItemByHeaderIdResponse>(GetallItemByIdResponse, "Exception", false));
                }


            }
        }
        public static string getSalesorderSataus(int statusId)
        {
            string status = string.Empty;

            try
            {
                switch (statusId)
                {
                    case 1:
                        status = "Created";
                        break;
                    case 2:
                        status = "Updated";
                        break;
                        //case 3:
                        //    Console.WriteLine("Wednesday");
                        //    break;
                        //case 4:
                        //    Console.WriteLine("Thursday");
                        //    break;
                        //case 5:
                        //    Console.WriteLine("Friday");
                        //    break;
                        //case 6:
                        //    Console.WriteLine("Saturday");
                        //    break;
                        //case 7:
                        //    Console.WriteLine("Sunday");
                        //    break;
                }

            }
            catch (Exception)
            {


            }
            return status;
        }
    }
}